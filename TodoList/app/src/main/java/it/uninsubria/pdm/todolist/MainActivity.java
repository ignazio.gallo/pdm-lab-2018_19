package it.uninsubria.pdm.todolist;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    private ListViewFragment listViewFragment = new ListViewFragment();
    private GridViewFragment gridViewFragment = new GridViewFragment();
    private ArrayList<TodoItem> todoItems;
    private ArrayAdapter<TodoItem> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_linearlayout);

        // add FragmentOne to the fragment container
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, listViewFragment);
        fragmentTransaction.commit();

        // get the references to the views
        final EditText editText = findViewById(R.id.editText);
        final Button addButton = findViewById(R.id.button);
        final Button listButton = findViewById(R.id.listview_button);
        final Button gridButton = findViewById(R.id.gridview_button);

        // the array list containing the todo items
        todoItems = new ArrayList<>();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, todoItems);

        listViewFragment.setAdapter(adapter);
        gridViewFragment.setAdapter(adapter);
        listViewFragment.setTodoItems(todoItems);
        gridViewFragment.setTodoItems(todoItems);

        listButton.setOnClickListener(this);
        gridButton.setOnClickListener(this);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddItem(editText);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v(TAG, "onResume()");
        // open the DB and update the listview
        this.todoItems.clear();
        todoItems.addAll(DatabaseHelper.getInstance(this).getAllItems());
        adapter.notifyDataSetChanged();
    }


    private void onAddItem(EditText editText) {
        String todo = editText.getText().toString();
        if (todo.length()==0) {
            Toast.makeText(getApplicationContext(),
                    "Empty ToDo string",
                    Toast.LENGTH_LONG).show();
            return;
        }
        TodoItem newTodo = new TodoItem(todo);
        todoItems.add(0, newTodo);
        DatabaseHelper.getInstance(this).insertItem(newTodo);
        adapter.notifyDataSetChanged();
        // clear text field
        editText.setText("");
        // hide the softkey
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getApplicationWindowToken(), 0);

        editText.clearFocus();
    }


    @Override
    public void onClick(View view) {
        if (view == findViewById(R.id.listview_button)) {
            // replace FragmentOne with FragmentTwo
            FragmentManager fm = getFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, listViewFragment);
            fragmentTransaction.commit();

        } else if (view == findViewById(R.id.gridview_button)){
            // replace FragmentTwo with FragmentOne
            FragmentManager fm = getFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, gridViewFragment);
            fragmentTransaction.commit();
        }
    }
}
