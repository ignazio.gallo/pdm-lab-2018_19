package it.uninsubria.pdm.todolist;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class TodoItem {
    private String todo;  // il task
    private GregorianCalendar createOn; // la data di creazione del task
    private int id;

    public TodoItem() {
    }

    public TodoItem(String todo) {
        this.todo = todo;
        this.createOn = new GregorianCalendar();
    }

    @Override
    public String toString() {
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy", Locale.ITALIAN);
        String currentDate =  fmtOut.format(createOn.getTime());
        return currentDate + ":\n >> " + todo;
    }


    public int getId() {
        return id;
    }

    public void setId(int parseInt) {
        this.id = parseInt;
    }

    public void setTask(String string) {
        this.todo = string;
    }

    public void setDate(GregorianCalendar createOn) {
        this.createOn = createOn;
    }

    public String getTask() {
        return this.todo;
    }

    public GregorianCalendar getDate() {
        return createOn;
    }
}
