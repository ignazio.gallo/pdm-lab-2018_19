package it.uninsubria.pdm.todolist;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListViewFragment extends Fragment {
    private ListView listView = null;
    ArrayAdapter<TodoItem> adapter = null;
    private ArrayList<TodoItem> todoItems = null;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_listview, container, false);
        listView = view.findViewById(R.id.list_view);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position,
                                           final long itemId) {
                onLongClick(position);
                return true;
            }
        });

        if (adapter!= null)
            listView.setAdapter(adapter);
        return view;
    }

    private void onLongClick(final int position) {
        // get the item delected for delation
        TodoItem item = (TodoItem) listView.getItemAtPosition(position);
        Toast.makeText(getActivity(), "Deleted Item " + position + " " + item.toString(),
                Toast.LENGTH_LONG).show();
        // delete the item from the DB
        DatabaseHelper.getInstance(getActivity()).deleteItem(item);
        // delete the item from the listview data structure
        todoItems.remove(position);
        // notify the data set has changed to the listview adapter
        adapter.notifyDataSetChanged();
    }


    public void setAdapter(ArrayAdapter<TodoItem> adapter){
        this.adapter = adapter;
        if (listView!= null)
            listView.setAdapter(adapter);
    }

    public void setTodoItems(ArrayList<TodoItem> todoItems) {
        this.todoItems = todoItems;
    }
}
