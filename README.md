# Corso di Programmazione di Dispositivi Mobili  
**a.a. 2018-2019**  
**Università degli Studi dell'Insubria**

Questo repository contiene i progetti sviluppati in laboratorio tramite l'uso di AndroidStudio.

- 01-MyFirstApp  
  primo progetto di laboratorio per la creazione di una App con una Activity che invia un SMS.